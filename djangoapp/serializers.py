from rest_framework import serializers
from .models import ShopDetail,SupplierInfo



class ShopSerializer(serializers.ModelSerializer):
    class Meta:
        model = ShopDetail
        fields = ['shop_name']

class SupplierSerializer(serializers.ModelSerializer):
    shop_detail = serializers.StringRelatedField(many=True,read_only=True)
    class Meta:
        model = SupplierInfo
        fields = ['suppler_name','address','shop_detail']

