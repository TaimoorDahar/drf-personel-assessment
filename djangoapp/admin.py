from django.contrib import admin
from .models import *
# Register your models here.

@admin.register(SupplierInfo)
class SupplierAdmin(admin.ModelAdmin):
    list_display = ('suppler_name','address',)

admin.site.register(Fooditems)
admin.site.register(ShopDetail)
