from django.shortcuts import render
from rest_framework.response import Response
from .models import *
from .serializers import ShopSerializer,SupplierSerializer
from rest_framework import status
from rest_framework import viewsets
from django.db.models import Value
from django.db.models.aggregates import Max

# Create your views here.


class FoodSuplier(viewsets.ViewSet):
    def list(self,request):
        sup = SupplierInfo.objects.all()
        search = request.GET.get('search')
        search = self.request.query_params.get('search')
        if search is not None:
            sup = SupplierInfo.objects.filter(shop_detail=search)
        serializer = SupplierSerializer(sup,many=True)
        return Response(serializer.data)
    
    def retrieve(self,request,pk=None):
        id = pk
        if id is not None:
            sup = SupplierInfo.objects.get(id=id)
            serializer = SupplierSerializer(sup)
            return Response(serializer.data)
    
    def create(self,request):
        serializer = SupplierInfo(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Reponse({'msg':'Data Created'},status=status.HTTP_201_CREATED)
        return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)

        def update(self,request,pk=None):
            id = pk
            sup = SupplierInfo.objects.get(id=id)
            serializer = SupplierSerializer(sup,data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response({'msg':'Complete Data Updated'})
            return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)

        def partial_update(self,request,pk=None):
            id = pk
            sup = SupplierInfo.objects.get(id=id)
            serializer = SupplierSerializer(sup,data=request.data,partial=True)
            if serializer.is_valid():
                serializer.save()
                return Response({'msg':'Partial Data Updated'})
            return Response(serializer.errors)
            
        def destroy(self,request,pk=None):
            id = pk
            sup = SupplierInfo.objects.get(id=id)
            sup.delete()
            return Response({'msg':'Data Deleted'})




