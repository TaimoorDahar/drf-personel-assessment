from django.db import models


class Fooditems(models.Model):
    food_items = models.CharField(max_length=255)
    def __str__(self):
        return self.food_items

class ShopDetail(models.Model):
    shop_name = models.CharField(max_length=255)
    food_items = models.ManyToManyField(Fooditems)
    
    def __str__(self):
        return self.shop_name

# Create your models here.
class SupplierInfo(models.Model):
    suppler_name = models.CharField(max_length=255)
    address = models.CharField(max_length=355)
    shop_detail = models.ManyToManyField(ShopDetail)

    def __str__(self):
        return self.suppler_name
