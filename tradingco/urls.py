from django.contrib import admin
from django.urls import path,include
from djangoapp import views
from rest_framework.routers import DefaultRouter

router = DefaultRouter()

# Registering Routers
router.register('supplier', views.FoodSuplier,basename='suplier')

urlpatterns = [
    path('admin/', admin.site.urls),
    path('',include(router.urls)),
]
